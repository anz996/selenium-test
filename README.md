# Selenium testovi

  

Najpre je neophodno pokrenuti chromedriver

```
chromedriver --host=localhost --port=4444
```

  

## 1. Pornhub

  
### Zahtevi

1.  Go to [https://www.pornhub.com/](https://www.pornhub.com/)
    
2.  Search “anal”
    
3.  Get the following info from 1st 500 vids:
    
    1.  Vid name
        
    2.  Vid URL
        
    3.  Uploader name
        
    4.  Likes/dislikes
        
    5.  1st 3 categories
        
    6.  Number of comments
        
4.  Export said info to CSV/Excel in the following format
    
5.  Upload php and the exported file for evaluation

### Realizacija

1. Kreiranje objekta klase PornhubTest unutar koje su smestene sve neophodne funkcije za izvrsenje testa
2. Inicijalizacija chromedriver-a
3. Pretraga po kategoriji i preuzimanje url-ova svih videa na prvih 25 stranica (500 videa)
4. Otvaranje jednog po jednog videa i preuzimanje podataka
5. Po zavrsetku obrade svih videa vrsi se export u .csv fajl

### Pokretanje
```
php scripts/pornhub.php
```

## 2. Imdb

Za ovaj test je neophodno pokrenuti i php webserver kako bi se izvrsio prikaz svih preuzetih videa

```
cd www

php -S localhost:8000
```

### Zahtevi

1. Open ‘inception’ on IMDB

  

	1. open google tab  
	2. fill out seach btn with ‘inception’  
	3. press enter
	4. open imdb link
	5. scrape data from edit video page
		data: writer, stars, grade, duration, genre, description, thumbnail, trailer
	6. save data to db  

2. Top Rated Movies

	1. open  top rated movies on imdb [https://www.imdb.com/chart/top/](https://www.imdb.com/chart/top/)  
	2. get  movies where imdb rating is less than 8.5
	3. open video one by one, scrape data (writer, stars, grade, duration, genre, description, thumbnail, trailer)
	4. save data to db
	5. display list of scraped videos, something like on [https://www.imdb.com/chart/top/](https://www.imdb.com/chart/top/), with thumbnail, title, description, grade...
	
Technologies: Php, mysql, selenium
Please explain project setup in steps.

### Realizacija

1. Kreiranje objekta klase ImdbTest unutar koje su smestene sve neophodne funkcije za izvrsenje testa
2. Inicijalizacija chromedriver-a
3. Pretraga filma 'Inception' na google-u, pronalezenje imdb linka, preuzimanje podataka o filmu i smestanje u bazu
4. Pretraga top filmova sa ocenom manjom od 8.5, otvaranje jednog po jednog, preuzimanje svih zahtevanih informacija i na kraju smestanje svih informacija u bazu

### Pokretanje

Pokretanje skripte
```
php scripts/imdb.php
```

Lista svih preuzetih filmova se moze proveriti na adresi
```
localhost:8000/view.php
```
