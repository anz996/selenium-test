<?php

use eftec\bladeone\BladeOne;
use eftec\routeone\RouteOne;
use Nette\Database\Connection;
require_once('../vendor/autoload.php');

$database = new Connection(
    'mysql:host=localhost;dbname=imdb',
    'root',
    '112358'
);

$videos = $database->query('SELECT * FROM videos');

$views = dirname( dirname(__FILE__) ) . '/views';
$cache = dirname( dirname(__FILE__) ) . '/cache';
$blade = new BladeOne($views,$cache,BladeOne::MODE_DEBUG); // MODE_DEBUG allows to pinpoint troubles.
echo $blade->run("movies",array("videos"=>$videos)); // it calls /views/hello.blade.php