<?php

    namespace Models {
        class ImdbVideo { 
            public $title;
            public $writer;
            public $stars;
            public $grade;
            public $duration;
            public $genres;
            public $description;
            public $thumbnail;
            public $trailer;

            function prepareForInsert() {
                return [
                    'title' => $this->title,
                    'writer' => $this->writer,
                    'stars' => $this->stars,
                    'grade' => $this->grade,
                    'duration' => $this->duration,
                    'genres' => $this->genres,
                    'description' => $this->description,
                    'thumbnail' => $this->thumbnail,
                    'trailer' => $this->trailer
                ];
            }
        }
    }
?>