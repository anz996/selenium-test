<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Nette\Database\Connection;
use Models\ImdbVideo;

require_once('models/ImdbVideo.php');
require_once('vendor/autoload.php');

class ImdbTest {
    public $driver;
    public $database;
    public $video_url;

    function init() {
        $host = 'http://localhost:4444';
        $capabilities = DesiredCapabilities::chrome();
        $this->driver = RemoteWebDriver::create($host, $capabilities);

        $this->database = new Connection(
            'mysql:host=localhost;dbname=imdb',
            'root',
            '112358'
        );
    }

    function closeDriver() {
        $this->driver->quit();
    }

    function findMovie($title) {
        $this->driver->get('https://www.google.com/en');
        
        $this->driver->findElement(WebDriverBy::cssSelector("input[title='Search']"))
            ->sendKeys($title)
            ->submit();

        $search_results = $this->driver->findElements(WebDriverBy::className('rc'));
        $url = null;

        foreach($search_results as $search_result) {
            $site = $search_result->findElement(WebDriverBy::tagName('a'))
                ->getAttribute('href');

            echo $site . "\xA";

            if (str_contains($site, 'www.imdb.com')) { 
                $url = $site;
                break;
            }
        }

        if ($url == null) {
            print_r('Nije pronadjen film.');
            $this->closeDriver();
            return;
        }

        return $url;
    }

    function getMovieData($url) {
        $this->driver->get($url);

        $video_data = new ImdbVideo();

        $this->driver->manage()->timeouts()->implicitlyWait(2);

        $video_data_container = $this->driver->findElement(WebDriverBy::id('title-overview-widget'));

        $title_wrapper = $video_data_container->findElement(WebDriverBy::className('title_wrapper'));
        $video_data->title = $title_wrapper->findElement(WebDriverBy::tagName('h1'))->getAttribute('innerText');
        $video_data->duration = $title_wrapper->findElement(WebDriverBy::tagName('time'))->getAttribute('innerText');
        $genres = $title_wrapper->findElement(WebDriverBy::className('subtext'))->findElements(WebDriverBy::tagName('a'));

        foreach($genres as $index=>$genre) {
            if(($index + 1) == count($genres))
                break;

            if ($video_data->genres != null)
                $video_data->genres .= ', ';
            $video_data->genres .= $genre->getAttribute('innerText');
        }

        $cs_items = $video_data_container->findElements(WebDriverBy::className('credit_summary_item'));

        foreach($cs_items as $cs_item) {
            $cs_item_text = $cs_item->getAttribute('innerText');

            if (str_contains($cs_item_text, 'Writer'))
                $video_data->writer = $cs_item->findElement(WebDriverBy::tagName('a'))->getAttribute('innerText');

            if (str_contains($cs_item_text, 'Stars')) {
                $stars = $cs_item->findElements(WebDriverBy::tagName('a'));

                foreach($stars as $index=>$star) {
                    if(($index + 1) == count($stars))
                        break;
                    if ($video_data->stars != null)
                        $video_data->stars .= ', ';
                    $video_data->stars .= $star->getAttribute('innerText');
                }
            }
        }

        $video_data->description = $video_data_container->findElement(WebDriverBy::className('summary_text'))->getAttribute('innerText');
        $video_data->grade = $video_data_container->findElement(WebDriverBy::cssSelector('div.ratingValue > strong > span'))->getAttribute('innerText');

        try {
            $video_data->thumbnail = $video_data_container->findElement(WebDriverBy::className('poster'))
                ->findElement(WebDriverBy::tagName('img'))
                ->getAttribute('src');
        } catch (\Exception $err) {
            $video_data->thumbnail = null;
        }

        try {
            $trailer_url = $video_data_container->findElement(WebDriverBy::className('videoPreview__videoContainer'))
                ->findElement(WebDriverBy::tagName('a'))
                ->getAttribute('href');

            $this->driver->get($trailer_url);

            $video_data->trailer = $this->driver->findElement(WebDriverBy::cssSelector('video.jw-video.jw-reset'))->getAttribute('src');
        } catch (\Exception $err) {
            $video_data->trailer = null;
        }

        return $video_data->prepareForInsert();
    }

    function getTopMovies($max_grade) {
        $this->driver->get('https://www.imdb.com/chart/top');

        $movies = $this->driver->findElement(WebDriverBy::className('lister-list'))->findElements(WebDriverBy::tagName('tr'));
        $urls = [];
        $movies_to_db = [];

        foreach($movies as $index=>$movie) {
            $rating = $movie->findElement(WebDriverBy::cssSelector('td.ratingColumn > strong'))
            ->getAttribute('innerText');

            $rating = floatval($rating);

            if ($rating <= $max_grade) {
                $url = $movie->findElement(WebDriverBy::cssSelector('td.titleColumn > a'))
                    ->getAttribute('href');

                array_push($urls, $url);
            }
        }

        echo "Total videos to process: " . (count($urls)) . "\xA";

        foreach($urls as $index=>$url) {
            $movie_data = $this->getMovieData($url);
            array_push($movies_to_db, $movie_data);
            system('clear');
            echo "Processed: ";
            echo $index + 1;
            echo "/";
            echo count($urls);
            echo "\xA";
        }

        return $movies_to_db;
    }

    function saveVideosToDB($videos) {
        $this->database->query('INSERT INTO videos', $videos);
    }

}

$imdb = new ImdbTest();
$imdb->init();
$url = $imdb->findMovie('Inception');
$video_data = $imdb->getMovieData($url);
$imdb->saveVideosToDB($video_data);
$movies = $imdb->getTopMovies(8.5);
$imdb->saveVideosToDB($movies);
$imdb->closeDriver();