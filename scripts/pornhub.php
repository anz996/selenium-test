<?php

namespace Facebook\WebDriver;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Models\PornhubVideo;

require_once('models/PornhubVideo.php');
require_once('vendor/autoload.php');

class PornhubTest {
    public $driver;
    public $video_urls = [];
    public $videos = [];

    function init() {
        $host = 'http://localhost:4444';
        $capabilities = DesiredCapabilities::chrome();
        $this->driver = RemoteWebDriver::create($host, $capabilities);
    }

    function closeDriver() {
        $this->driver->quit();
    }

    function getUrls($tag, $number_of_pages) {

        $this->driver->get('https://www.pornhub.com');
        
        $this->driver->findElement(WebDriverBy::id('searchInput'))
            ->sendKeys($tag)
            ->submit();
        
        for($i = 0; $i < $number_of_pages; $i++) {
            $this->driver->manage()->timeouts()->implicitlyWait(2);
        
            $videos_list_item = $this->driver->findElement(WebDriverBy::id('videoSearchResult'));
            $videos_on_page = $videos_list_item->findElements(WebDriverBy::className('videoPreviewBg'));
            foreach($videos_on_page as $video_on_page) {
                $href = $video_on_page->getAttribute('href');
                array_push($this->video_urls, $href);
            }
        
            $this->driver->findElement(WebDriverBy::linkText('Next'))
                ->click();
        }
    }

    function loopAllVideos() {
        foreach($this->video_urls as $video_url) {
            $this->getVideoData($video_url);
        }

        print_r($this->videos);
    }

    function getVideoData($url) {

        $url = $url;

        $this->driver->get($url);

        $video_data = new PornhubVideo();

        $video_data->name = $this->driver->findElement(WebDriverBy::className('inlineFree'))->getAttribute('innerText');
        $video_data->url = $url;
        $video_data->uploader_name = $this->driver->findElement(WebDriverBy::className('video-detailed-info'))
            ->findElement(WebDriverBy::className('usernameWrap'))
            ->findElement(WebDriverBy::className('bolded'))
            ->getAttribute('innerText');
        $votes_up = $this->driver->findElement(WebDriverBy::className('votesUp'))->getAttribute('innerText');
        $votes_down = $this->driver->findElement(WebDriverBy::className('votesDown'))->getAttribute('innerText');
        $video_data->likes_dislikes = $votes_up .= '/' . $votes_down;
        $video_data->number_of_comments = $this->driver->findElement(WebDriverBy::id('cmtWrapper'))
            ->findElement(WebDriverBy::tagName('h2'))
            ->findElement(WebDriverBy::tagName('span'))
            ->getAttribute('innerText');
        $categories = $this->driver->findElement(WebDriverBy::className('categoriesWrapper'))
            ->findElements(WebDriverBy::tagName('a'));

        foreach($categories as $index=>$category) {
            if($index > 2)
                break;
            if ($video_data->categories != null)
                $video_data->categories .= ', ';
            $video_data->categories .= $category->getAttribute('innerText');
        }

        $video_data->number_of_comments = str_replace(array('(', ')'), '', $video_data->number_of_comments);
        array_push($this->videos, $video_data);
        $this->driver->manage()->timeouts()->implicitlyWait(2);
    }

    function exportToCSV() {
        $fp = fopen('data/videos.csv', 'w');
        $fields = ['Name', 'URL', 'Uploader', 'Likes/dislikes', 'Categories', 'Number of comments'];
        fputcsv($fp, $fields); 

        foreach ($this->videos as $video) {
            fputcsv($fp, get_object_vars($video));
        }

        fclose($fp);
    }
}

$ph = new PornhubTest();
$ph->init();
$ph->getUrls('anal', 25);
// $ph->getVideoData('https://www.pornhub.com/view_video.php?viewkey=315880772');
$ph->loopAllVideos();
$ph->exportToCSV();
$ph->closeDriver();