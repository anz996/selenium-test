<html>
    <head>
        <title>Imdb Videos</title>
    </head>
    <body>
        <div class="container pt-3">
            <div class="row">
                <div class="col-8 offset-2">
                    <h2 class="text-center p-3 m-3">Lista filmova</h2>
                    @foreach ($videos as $video)
                    <div class="row border border-light">
                        <div class="col-2 p-0">
                            <img src="{{$video->thumbnail}}" width="64">
                        </div>
                        <div class="col-8 p-2 m-auto">
                            <h6 class="m-auto">{{$video->title}}</h6>
                            <!-- <h6>{{$video->description}}</h6> -->
                        </div>
                        <div class="col-2 text-right m-auto">
                            <strong>{{$video->grade}}</strong>
                            <svg width="20px" height="20px" viewBox="0 0 16 16" class="bi bi-star-fill mb-1 ml-2" fill="#ffc107" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    </body>
</html>